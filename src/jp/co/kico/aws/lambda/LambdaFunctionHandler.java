package jp.co.kico.aws.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class LambdaFunctionHandler implements RequestHandler<CustomParam, CustomParam> {

    @Override
    public CustomParam handleRequest(CustomParam input, Context context) {
    	CustomParam output = new CustomParam();
    	output.setUserId("OutputUser");
    	output.setPassword("OutputPassword");
    	
        context.getLogger().log("Input: " + input);
        context.getLogger().log("Ouput: " + output);
        return output;
    }

}
